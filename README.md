> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advancced Web Applications Development

## Christian Pelaez-Espinosa

### Assignment #2 Requirements:

*Three Parts*

1. Mysql Installation
2. Chpater Questions (Chs 5-6)

#### README.md file should include the following items:

*Screenshot of http://localhost:9999/hello (displays directory, needs index.html)![Screen Shot 2016-02-01 at 10.36.00 AM.png](https://bitbucket.org/repo/BKgqk6/images/3643441228-Screen%20Shot%202016-02-01%20at%2010.36.00%20AM.png)
*Screenshot of http://localhost:9999/hello/index.html
(Can rename "HelloHome.html" to "index.html")
![Screen Shot 2016-02-01 at 10.36.18 AM.png](https://bitbucket.org/repo/BKgqk6/images/4285277980-Screen%20Shot%202016-02-01%20at%2010.36.18%20AM.png)
* Screenshot of http://localhost:9999/hello/sayhello (invokes HelloServlet)
Note: /sayhello maps to HelloServlet.class (changed web.xml file)
![Screen Shot 2016-02-01 at 10.36.30 AM.png](https://bitbucket.org/repo/BKgqk6/images/950842807-Screen%20Shot%202016-02-01%20at%2010.36.30%20AM.png)

* Screenshot of http://localhost:9999/hello/querybook.html
* Screenshot of http://localhost:9999/hello/sayhi (invokes AnotherHelloServlet)

>
>
>
>

#### Assignment Screenshots:

*Screenshot of http://localhost:9999/hello (displays directory, needs index.html)




*Screenshot of http://localhost:9999/hello/HelloHome.html
(Can rename "HelloHome.html" to "index.html")





* Screenshot of http://localhost:9999/hello/sayhello (invokes HelloServlet)
Note: /sayhello maps to HelloServlet.class (changed web.xml file)




* Screenshot of http://localhost:9999/hello/querybook.html




* Screenshot of http://localhost:9999/hello/sayhi (invokes AnotherHelloServlet)


**Quiz Questions**

1) A JavaBean is a Java class that

c.provides get and set methods for all of its private instance variables that follow standard Java
naming conventions


2) A servlet is a Java class that extends the

c.HttpServlet class


3) Before you can use the core JSTL tags in a JSP, you must code a/an ________________ for the core JSTL library.

a.taglib directive

4) EL makes it easy to access the _______________ of JavaBeans.

d.properties

5) If the following JSP tag is coded in a JSP named index.jsp, what happens when the
reports.jsp file is updated?
<jsp:include page="includes/reports.jsp" />

b. the changes appear for subsequent requests for the index.jsp file

6) In the code that follows, DiagnoseTrouble
<servlet-mapping>
 <servlet-name>DiagnoseTrouble</servlet-name>
 <url-pattern>/diagnostics/diagnoseTrouble</url-pattern>
</servlet-mapping>

b. is the internal name for the servlet class that’s used in the deployment descriptor


7) In the code that follows, the servlet is mapped to
<servlet-mapping>
 <servlet-name>DiagnoseTrouble</servlet-name>
 <url-pattern>/diagnostics/diagnoseTrouble</url-pattern>
</servlet-mapping>

a.the /diagnostics/diagnoseTrouble URL of the document root directory



8) The advantages of EL are

a.more elegant and compact syntax
b.allows you to access nested properties
c.does a better job of handling null values
d.more functionality
e.all of the above  --- Answer

9) The following code from a servlet class
System.out.println("Check with system administrator");

a.prints text to the standard output stream


10) The following code
String[] mailTypes = request.getParameterValues("mail");

c.returns all values of the mail parameter or null if none exist


11) The init method of a servlet class is called

a.the first time a servlet is requested but before the service method

12) To define a/an ________________ initialization parameter that’s available to all servlets
in the web application, you code a/an ________________ element.

b.servlet, init-param

13) To define a/an ________________ initialization parameter that’s available to a specific servlet, you can code a/an ________________ element within a servlet element.

d.context, context-param

14) When the doGet method of a servlet is called, it receives

b.the request and response objects


15) Whenever you us EL, you begin by coding a

b.dollar sign


16) Which of the following is a valid JSP comment?

c. <%-- comment --%>


17) Which of the following is a valid Java comment?

a. // comment


18) Which of the following statements gets the value of the parameter named occupation?

b. String occupation = request.getParameter("occupation")

19) You can use ________________ to perform data validation on the server.

b.a servlet

20) You can use ________________, or ________________ to perform data validation on the client.

c.JavaScript, jQuery


21) ________________ provides a compact syntax that lets you get data from a JavaBean that has been stored as an attribute of the request.

b.Expression Language (EL)

22) ___________________ provides tags for common tasks that need to be performed in JSPs.

a.The JSP Standard Tag Library (JSTL)